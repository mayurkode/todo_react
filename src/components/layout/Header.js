import React from 'react';
import { Link } from 'react-router-dom'
function Header() {

    return (
        <header style={headerStyles}>
            <h1>Todo List</h1>
            <Link to="/" style={linkStyles}> Home </Link> | <Link to="/about" style={linkStyles}> About </Link>
        </header>
    )
}

const headerStyles = {
    backgroundColor:'#333',
    color:'#fff',
    padding: '10px',
    textAlign:'center'
}

const linkStyles = {
    color:'#fff',
    textDecoration: 'none'
}

export default Header;