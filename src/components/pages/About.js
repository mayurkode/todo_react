import React, { Component } from 'react'

export default class About extends Component {
    render() {
        return (
            <>
                <h1>About</h1>
                <p> React Todo List application with following React Concepts:</p>
                <ul>
                    <li>Components</li>
                    <li>Event Drilling</li>
                    <li>state</li>
                    <li>events</li>
                    <li>styles</li>
                    <li>Routing</li>
                    <li>uuid</li>
                </ul>
            </>
        )
    }
}


