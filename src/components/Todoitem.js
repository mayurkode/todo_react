import React, { Component } from 'react'
import PropTypes from 'prop-types';
export class Todoitem extends Component {

    getStyle = () => {
        return {
            backgroundColor: '#f4f4f4',
            marginBottom: '4px',
            padding: '10px',
            border: '1px #ccc dotted',
            textDecoration: this.props.todo.completed ? 'line-through' : 'none',
            fontStyle: this.props.todo.completed ? 'italic' : 'normal' 
        }
    }


    render() {

        const { id, title, completed } = this.props.todo;
        return (
            <div style= {this.getStyle()}>
                
                <p> <input type="checkbox" checked={completed} onChange={this.props.markComplete.bind( this, id )}  style={ checkStyle }></input>
                    {title}
                    <button style= { btnStyle } onClick={this.props.deleteTodo.bind(this, id)}> X </button>
                </p>
            </div>
        )
    }
}

// PropTypes
Todoitem.prototypes = {
    todo : PropTypes.object.isRequired
}

const btnStyle = {
    backgroundColor: 'red',
    border: '1px solid red',
    color: 'white',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right',
    padding: '4px 4px'
}

const checkStyle = {
    marginRight: '10px'
}
export default Todoitem;
