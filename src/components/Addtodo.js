import React, { Component } from 'react'



export default class Addtodo extends Component {

    state = { 
        title: ''
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value});
    }

    onAddItem = (title) => {
        this.props.addTodo(this.state.title);
    }
    render() {
        return (
            <div>
                <form>
                    <input type="text"
                      name="title"
                      placeholder="Enter Todo Item"
                      value = { this.state.title}
                      onChange={this.onChange}></input>
                    <button type="button" onClick={this.onAddItem}> Add </button>
                </form>
                
            </div>
        )
    }
}

