import React, {
  Component
} from 'react';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Todos from './components/Todos';
import Header from './components/layout/Header';
import Addtodo from './components/Addtodo';
import About from './components/pages/About';
import { v4 as uuid} from 'uuid';
class App extends Component {

  state = {
    todos : [
      {
        id: uuid(),
        title: 'Take out the trash',
        completed: false
      },
      {
        id: uuid(),
        title: 'Dinner at Hotel',
        completed: false
      },
      {
        id: uuid(),
        title: 'Meeting on skype',
        completed: true
      }
    ],

    data : 'done'
  }



  markComplete = (id) => {
   this.setState ({ todos : this.state.todos.map(todo => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
      return todo;
    })
   });

   this.setState( { data : 'kode'} );

   console.log(this.state);

  }

  deleteTodo = (id) => {
    console.log('delete item', id);

    this.setState ( { todos : this.state.todos.filter( todo => {

      if (todo.id !== id){
        return todo;
      } 
      // eslint-disable-next-line array-callback-return
      return;
    })

    });
  }

  addTodo = (title) => {
    // console.log(title);
    const newTodo = {
      id: uuid(),
      title: title,
      completed: false
    }

    this.setState({ todos: [...this.state.todos, newTodo] });
  }

  render() {
    return (

      <Router>
            <div className="App">
              <main>

              <Header></Header>
              
              <Route exact path="/" render={ props => (
                <>
                  <Addtodo addTodo={this.addTodo}></Addtodo>
                  <Todos todos={this.state.todos} markComplete= { this.markComplete } deleteTodo={this.deleteTodo}></Todos>
                </>
              )}/>

              <Route path="/about" component={About}/>

              </main>
             
                 
            </div>
      </Router>
      

    )
  }

}

export default App;